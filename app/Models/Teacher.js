'use strict'

const Model = use('Model')

class Teacher extends Model {

  village() {
    return this.belongsTo('App/Models/Village')
  }
}

module.exports = Teacher
