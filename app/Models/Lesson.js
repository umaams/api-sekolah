'use strict'

const Model = use('Model')

class Lesson extends Model {

  curriculums() {
    return this
      .belongsToMany('App/Models/Curriculum', 'lesson_id',
        'curriculum_id',
        'id',
        'id')
      .pivotTable('curriculum_details')
  }
}

module.exports = Lesson
