'use strict'

const Model = use('Model')

class Curriculum extends Model {
  static get table() {
    return 'curriculums'
  }

  lessons() {
    return this
      .belongsToMany('App/Models/Lesson', 'curriculum_id',
        'lesson_id',
        'id',
        'id')
      .pivotTable('curriculum_details')
  }
}

module.exports = Curriculum
