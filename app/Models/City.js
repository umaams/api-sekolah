'use strict'

const Model = use('Model')

class City extends Model {

  province() {
    return this.belongsTo('App/Models/Province')
  }
}

module.exports = City
