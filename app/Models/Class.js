'use strict'

const Model = use('Model')

class Class extends Model {

  grade() {
    return this.belongsTo('App/Models/Grade')
  }

  students() {
    return this
      .belongsToMany('App/Models/Student', 'class_id',
        'student_id',
        'id',
        'id')
      .pivotTable('class_students')
  }

  guardians() {
    return this
      .belongsToMany('App/Models/Teacher', 'class_id',
        'teacher_id',
        'id',
        'id')
      .pivotTable('class_guardians')
  }

  teachers() {
    return this
      .belongsToMany('App/Models/Teacher', 'class_id',
        'teacher_id',
        'id',
        'id')
      .pivotTable('class_guardians')
  }

  class_guardian() {
    return this.hasOne('App/Models/ClassGuardian', 'id', 'class_id')
  }
}

module.exports = Class
