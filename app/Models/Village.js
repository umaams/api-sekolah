'use strict'

const Model = use('Model')

class Village extends Model {

  district() {
    return this.belongsTo('App/Models/District')
  }
}

module.exports = Village
