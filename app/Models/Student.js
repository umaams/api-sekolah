'use strict'

const Model = use('Model')

class Student extends Model {

  village() {
    return this.belongsTo('App/Models/Village')
  }

  classes() {
    return this
      .belongsToMany('App/Models/Class', 'student_id',
        'class_id',
        'id',
        'id')
      .pivotTable('class_students')
  }
}

module.exports = Student
