'use strict'

const Model = use('Model')

class District extends Model {

  city() {
    return this.belongsTo('App/Models/City')
  }
}

module.exports = District
