'use strict'

const Model = use('Model')

class Grade extends Model {

  classes() {
    return this.hasMany('App/Models/Class')
  }

  lessons() {
    return this
      .belongsToMany('App/Models/Lesson', 'grade_id',
        'lesson_id',
        'id',
        'id')
      .pivotTable('curriculum_details')
  }
}

module.exports = Grade
