'use strict'

const Model = use('Model')

class ClassGuardian extends Model {

  teacher() {
    return this.belongsTo('App/Models/Teacher')
  }
}

module.exports = ClassGuardian
