'use strict'

const District = use('App/Models/District')
class CityController {

  async showDistricts({ request, params, response }) {
    const districts = await District.query().where('city_id', params.id).orderBy('name')
    return response.ok({ status: true, data: { districts: districts } })
  }
}

module.exports = CityController
