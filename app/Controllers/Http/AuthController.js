'use strict'

const { validateAll } = use('Validator')
const User = use('App/Models/User')

class AuthController {

  async login ({ auth, request, response }) {
    const rules = {
      email: 'required',
      password: 'required'
    }
    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({
        status: false,
        request: request,
        errors: validation.messages()
      })
    } else {
      const email = request.input('email', null)
      const password = request.input('password', null)
      const secret = await auth.attempt(email, password)
      return response.ok({ status: true, data: { auth: secret }})
    }
  }
}

module.exports = AuthController
