'use strict'

const { validateAll } = use('Validator')
const Class = use('App/Models/Class')

class ClassController {

  async index({ request, response }) {
    let status = true
    let error = ''

    const page = request.input('page', 1);
    const per_page = request.input('per_page', 10)
    const select = request.input('select', null)
    const filter = request.input('filter', null)    
    const sort = request.input('sort', null)
    const include = request.input('include', null)    

    let classes = []
    let query = Class.query().whereNull('deleted_at')
    if (filter != null) {
      query.where(function () {
        this.orWhereRaw('LOWER(name) LIKE ?', ['%' + filter.toLowerCase() + '%'])
      })
    }
    if (include != null) {
      const arrinclude = include.split(',')
      if (arrinclude.indexOf('grade') != -1) {
        query.with('grade')
      }
    }
    if (select != null) {
      const arrselect = select.split(',')
      query.select(arrselect)
    }
    if (sort != null) {
      const arrsort = sort.split(',')
      arrsort.forEach(element => {
        query.orderBy(element.split('|')[0].toLowerCase(), element.split('|').length == 2 ? element.split('|')[1].toLowerCase() : 'asc')
      })
    }
    classes = await query.paginate(page, per_page).catch(function (error) {
      status = false
      error = 'Server Internal Error:' + error.message
    })
    return response.ok({
      status: status,
      error: error,
      data: {
        classes: classes
      }
    })
  }

  async show({ request, params, response }) {
    const kelas = await Class.query().whereNull('deleted_at').where('id', params.id).first()
    response.ok({ status: true, error: '', data: { class: kelas } })
  }

  async store({ request, response }) {
    let status = false
    let error = ''

    const rules = {
      name: 'required',
      grade_id: 'required|number'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      const kelas = await Class.create({
        name: request.input('name'),
        grade_id: request.input('grade_id'),
        active: request.input('active', '0')
      })
      response.ok({ status: true, error: '', class: kelas })
    }
  }

  async update({ params, request, response }) {
    const rules = {
      name: 'required',
      grade_id: 'required|number'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      await Class.query().where('id', params.id).update({
        name: request.input('name'),
        grade_id: request.input('grade_id'),
        active: request.input('active', '0')        
      })
      response.ok({ status: true })
    }
  }

  async destroy({ params, response }) {
    const kelas = await Class.query().whereNull('deleted_at').where('id', params.id).update({
      'deleted_at': moment().format('YYYY-MM-DD HH:mm:ss')
    })
    response.ok({ status: true })
  }
}

module.exports = ClassController
