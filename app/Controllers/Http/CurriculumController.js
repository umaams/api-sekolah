'use strict'

const { validateAll } = use('Validator')
const Curriculum = use('App/Models/Curriculum')
const CurriculumDetail = use('App/Models/CurriculumDetail')
const Grade = use('App/Models/Grade')
const Lesson = use('App/Models/Lesson')
const moment = require('moment')

class CurriculumController {

  async index ({ request, params, response }) {
    const curriculums = await Curriculum.all()
    response.ok({ status: true, data: { curriculums: curriculums } })
  }

  async show({ request, params, response }) {
    const curriculum = await Curriculum.findOrFail(params.id)
    response.ok({ status: true, error: '', data: { curriculum: curriculum } })
  }

  async store({ request, response }) {
    let status = false
    let error = ''

    const rules = {
      name: 'required'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({
        status: false,
        errors: validation.messages()
      })
    } else {
      const curriculum = await Curriculum.create({
        name: request.input('name'),
        active: request.input('active', '0')
      })
      response.ok({
        status: true,
        error: '',
        curriculum: curriculum
      })
    }
  }

  async showDetails({ request, params, response }) {
    const grades = await Grade.query().with('lessons', (builder) => {
      return builder.where('curriculum_id', params.id).orderBy('name')
    }).select('id', 'name').fetch()
    return response.ok({
      status: true,
      error: '',
      data: {
        grades: grades
      }
    })
  }

  async attachLesson({ request, params, response }) {

    const rules = {
      grade_id: 'required',
      lesson_id: 'required|array'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      const curriculum_id = params.curriculum_id
      const grade_id = request.input('grade_id', null)
      const lesson_id = request.input('lesson_id', [])
      const curriculum = await Curriculum.find(curriculum_id)

      for (let index = 0; index < lesson_id.length; index++) {
        const lesson = await Lesson.find(lesson_id[index])

        await lesson.curriculums().attach(curriculum_id, (row) => {
          row.grade_id = grade_id
        })
      }
    }

    return response.ok({ status: true })    
  }

  async detachLesson({ request, params, response }) {

    const rules = {
      grade_id: 'required',      
      lesson_id: 'required|array'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      const grade_id = request.input('grade_id', null)
      const lesson_id = request.input('lesson_id', [])

      await CurriculumDetail.query().where('curriculum_id', params.curriculum_id).where('grade_id', grade_id).whereIn('lesson_id', lesson_id).delete()
    }

    return response.ok({ status: true })
  }
}

module.exports = CurriculumController
