'use strict'

const { validate } = use('Validator')
const SchoolYear = use('App/Models/SchoolYear')
const Grade = use('App/Models/Grade')
const Class = use('App/Models/Class')
const moment = require('moment')

class GradeController {

  async index({ request, response }) {
    let status = true
    let error = ''

    const select = request.input('select', null)
    const sort = request.input('sort', null)

    let grades = []
    let query = Grade.query()
    if (select != null) {
      const arrselect = select.split(',')
      query.select(arrselect)
    }
    if (sort != null) {
      const arrsort = sort.split(',')
      arrsort.forEach(element => {
        query.orderBy(element.replace('-', '').toLowerCase(), (element.indexOf('-') == -1) ? 'asc' : 'desc')
      })
    }
    grades = await query.catch(function (error) {
      status = false
      error = 'Server Internal Error:' + error.message
    })
    return response.ok({
      status: status,
      error: error,
      data: {
        grades: grades
      }
    })
  }

  async showClassStudent ({ request, params, response }) {
    const active_school_year = await SchoolYear.query().where('active', '1').first()
    
    const classes = await Class.query().with('students', (builder) => {
      return builder.where('school_year_id', active_school_year.id).orderBy('name')
    }).select('id', 'name', 'grade_id').where('grade_id', params.id).fetch()
    return response.ok({
      status: true,
      error: '',
      data: {
        classes: classes
      }
    })
  }

  async showClassGuardian ({ request, response }) {
    const active_school_year = await SchoolYear.query().where('active', '1').first()

    const grades = await Grade.query().with('classes', (builder) => {
      return builder.with('class_guardian', (builder) => {
        return builder.with('teacher').where('school_year_id', active_school_year.id)
      })
    }).fetch()
    return response.ok({
      status: true,
      error: '',
      data: {
        grades: grades
      }
    })
  }
}

module.exports = GradeController
