'use strict'

const Province = use('App/Models/Province')
const City = use('App/Models/City')
class ProvinceController {

  async index ({request, response}) {
    const provinces = await Province.query().orderBy('name')
    return response.ok({status: true, data: { provinces: provinces } } )  
  }

  async showCities({ request, params, response }) {
    const cities = await City.query().where('province_id', params.id).orderBy('name')
    return response.ok({ status: true, data: { cities: cities } })
  }
}

module.exports = ProvinceController
