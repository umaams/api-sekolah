'use strict'

const { validateAll } = use('Validator')
const SchoolYear = use('App/Models/SchoolYear')
const Class = use('App/Models/Class')
const Student = use('App/Models/Student')

class ClassStudentController {

  async attachStudent({ request, params, response }) {

    const rules = {
      student_id: 'required|array'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      const student_id = request.input('student_id', [])
      const active_school_year = await SchoolYear.query().where('active', '1').first()
      
      for (let index = 0; index < student_id.length; index++) {
        const student = await Student.find(student_id[index])

        await student.classes().sync(params.id, (row) => {
          row.school_year_id = active_school_year != null ? active_school_year.id : 0
        })
      }
    }

    return response.ok({ status: true })    
  }

  async detachStudent({ request, params, response }) {

    const rules = {
      student_id: 'required|array'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      const student_id = request.input('student_id', [])
      const kelas = await Class.find(params.id)
      const active_school_year = await SchoolYear.query().where('active', '1').first()

      await kelas.students().detach(student_id)
    }

    return response.ok({ status: true })
  }
}

module.exports = ClassStudentController
