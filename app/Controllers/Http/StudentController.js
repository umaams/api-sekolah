'use strict'

const { validateAll } = use('Validator')
const SchoolYear = use('App/Models/SchoolYear')
const Student = use('App/Models/Student')
const moment = require('moment')

class StudentController {

  async index({ request, response }) {
    let status = true
    let error = ''

    const page = request.input('page', 1);
    const per_page = request.input('per_page', 10)
    const select = request.input('select', null)
    const filter = request.input('filter', null) 
    const sort = request.input('sort', null)

    let students = []
    let query = Student.query().whereNull('deleted_at')
    if (filter != null) {
      query.where(function () {
        this.orWhereRaw('LOWER(nis) LIKE ?', ['%' + filter.toLowerCase() + '%'])
        this.orWhereRaw('LOWER(name) LIKE ?', ['%' + filter.toLowerCase() + '%'])
        this.orWhereRaw('LOWER(guardian_name) LIKE ?', ['%' + filter.toLowerCase() + '%'])
      })
    }
    if (select != null) {
      const arrselect = select.split(',')
      query.select(arrselect)
    }
    if (sort != null) {
      const arrsort = sort.split(',')
      arrsort.forEach(element => {
        query.orderBy(element.split('|')[0].toLowerCase(), element.split('|').length == 2 ? element.split('|')[1].toLowerCase() : 'asc')
      })
    }
    students = await query.paginate(page, per_page).catch(function (error) {
      status = false
      error = 'Server Internal Error:' + error.message
    })
    return response.ok({
      status: status,
      error: error,
      data: {
        students: students
      }
    })
  }
  
  async show({ request, params, response }) {
    const include = request.input('include', null)

    const query = Student.query()
    if (include != null) {
      const arrinclude = include.split(',')
      if (arrinclude.indexOf('province') != -1) {
        query.with('village.district.city.province')
      }
    }
    const student = await query.whereNull('deleted_at').where('id', params.id).first()
    response.ok({status: true, error: '', data: { student: student }})
  }
  
  async store ({ request, response }) {
    let status = false
    let error = ''
    
    const rules = {
      nis: 'required|unique:students,nis',
      name: 'required'
    }
    
    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      const student = await Student.create({
        nis: request.input('nis', null),
        name: request.input('name', null),
        sex: (request.input('sex', null) != '') ? request.input('sex', null) : null,
        birth_place: request.input('birth_place', null),
        birth_date: (request.input('birth_date', null) != '') ? moment(request.input('birth_date', null)).format('YYYY-MM-DD') : null,
        address: request.input('address', null),
        village_id: (request.input('village_id', null) != '') ? request.input('village_id', null) : null,
        zip_code: request.input('zip_code', null),
        religion_id: (request.input('religion_id', null) != '') ? request.input('religion_id', null) : null,
        father_name: request.input('father_name', null),
        father_job: request.input('father_job', null),
        father_address: request.input('father_address', null),
        father_phone: request.input('father_phone', null),
        mother_name: request.input('father_name', null),
        mother_job: request.input('father_job', null),
        mother_address: request.input('father_address', null),
        mother_phone: request.input('father_phone', null),
        guardian_name: request.input('guardian_name', null),
        guardian_job: request.input('guardian_job', null),
        guardian_address: request.input('guardian_address', null),
        guardian_phone: request.input('guardian_phone', null),
        active: request.input('active', '0'),
        registered_at: (request.input('registered_at', null) != '') ? moment(request.input('registered_at', null)).format('YYYY-MM-DD') : null
      })
      response.ok({status: true, error: '', student: student})
    }
  }
  
  async update({ params, request, response }) {
    const rules = {
      nis: 'unique:students,nis,id,' + params.id,
      name: 'required'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      await Student.query().where('id', params.id).update({
        nis: request.input('nis', null),
        name: request.input('name', null),
        sex: (request.input('sex', null) != '') ? request.input('sex', null) : null,
        birth_place: request.input('birth_place', null),
        birth_date: (request.input('birth_date', null) != '') ? moment(request.input('birth_date', null)).format('YYYY-MM-DD') : null,
        address: request.input('address', null),
        village_id: (request.input('village_id', null) != '') ? request.input('village_id', null) : null,
        zip_code: request.input('zip_code', null),
        religion_id: (request.input('religion_id', null) != '') ? request.input('religion_id', null) : null,
        father_name: request.input('father_name', null),
        father_job: request.input('father_job', null),
        father_address: request.input('father_address', null),
        father_phone: request.input('father_phone', null),
        mother_name: request.input('father_name', null),
        mother_job: request.input('father_job', null),
        mother_address: request.input('father_address', null),
        mother_phone: request.input('father_phone', null),
        guardian_name: request.input('guardian_name', null),
        guardian_job: request.input('guardian_job', null),
        guardian_address: request.input('guardian_address', null),
        guardian_phone: request.input('guardian_phone', null),
        active: request.input('active', '0'),        
        registered_at: (request.input('registered_at', null) != '') ? moment(request.input('registered_at', null)).format('YYYY-MM-DD') : null
      })
      response.ok({ status: true })
    }
  }
  
  async destroy({ params, response }) {
    const student = await Student.query().where('id', params.id).update({
      'deleted_at': moment().format('YYYY-MM-DD HH:mm:ss')
    })
    response.ok({ status: true })
  }

  async showClassNone({ request, params, response }) {
    const page = request.input('page', 1);
    const per_page = request.input('per_page', 10)
    const select = request.input('select', null)
    const filter = request.input('filter', null)
    const sort = request.input('sort', null) 
    const active_school_year = await SchoolYear.query().where('active', '1').first()
    
    const query = Student.query().where('deleted_at', null).whereDoesntHave('classes', (builder) => {
      builder.where('school_year_id', active_school_year != null ? active_school_year.id: 0)
    })
    if (filter != null) {
      query.where(function () {
        this.orWhereRaw('LOWER(nis) LIKE ?', ['%' + filter.toLowerCase() + '%'])
        this.orWhereRaw('LOWER(name) LIKE ?', ['%' + filter.toLowerCase() + '%'])
        this.orWhereRaw('LOWER(guardian_name) LIKE ?', ['%' + filter.toLowerCase() + '%'])
      })
    }
    if (select != null) {
      const arrselect = select.split(',')
      query.select(arrselect)
    }
    if (sort != null) {
      const arrsort = sort.split(',')
      arrsort.forEach(element => {
        query.orderBy(element.split('|')[0].toLowerCase(), element.split('|').length == 2 ? element.split('|')[1].toLowerCase() : 'asc')
      })
    }
    const students = await query.paginate(page, per_page).catch(function (error) {
      status = false
      error = 'Server Internal Error:' + error.message
    })

    response.ok({
      status: true,
      data: {
        students: students
      }
    })
  }
}

module.exports = StudentController
