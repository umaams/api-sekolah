'use strict'

const { validateAll } = use('Validator')
const Teacher = use('App/Models/Teacher')
const moment = require('moment')

class TeacherController {

  async index({ request, response }) {
    let status = true
    let error = ''

    const page = request.input('page', 1);
    const per_page = request.input('per_page', 10)
    const select = request.input('select', null)
    const filter = request.input('filter', null)    
    const sort = request.input('sort', null)

    let teachers = []
    let query = Teacher.query().whereNull('deleted_at')
    if (filter != null) {
      query.where(function () {
        this.orWhereRaw('LOWER(nuptk) LIKE ?', ['%' + filter.toLowerCase() + '%'])
        this.orWhereRaw('LOWER(name) LIKE ?', ['%' + filter.toLowerCase() + '%'])
      })
    }
    if (select != null) {
      const arrselect = select.split(',')
      query.select(arrselect)
    }
    if (sort != null) {
      const arrsort = sort.split(',')
      arrsort.forEach(element => {
        query.orderBy(element.split('|')[0].toLowerCase(), element.split('|').length == 2 ? element.split('|')[1].toLowerCase() : 'asc')
      })
    }
    teachers = await query.paginate(page, per_page).catch(function (error) {
      status = false
      error = 'Server Internal Error:' + error.message
    })
    return response.ok({
      status: status,
      error: error,
      data: {
        teachers: teachers
      }
    })
  }

  async show({ request, params, response }) {
    const include = request.input('include', null)

    const query = Teacher.query()
    if (include != null) {
      const arrinclude = include.split(',')
      if (arrinclude.indexOf('province') != -1) {
        query.with('village.district.city.province')
      }
    }
    const teacher = await query.whereNull('deleted_at').where('id', params.id).first()
    response.ok({ status: true, error: '', data: { teacher: teacher } })
  }

  async store({ request, response }) {
    let status = false
    let error = ''

    const rules = {
      nuptk: 'required|unique:teachers,nuptk',
      name: 'required'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      const teacher = await Teacher.create({
        nuptk: request.input('nuptk'),
        name: request.input('name'),
        sex: request.input('sex'),
        birth_place: request.input('birth_place'),
        birth_date: (request.input('birth_date', null) != '') ? moment(request.input('birth_date', null)).format('YYYY-MM-DD') : null,
        address: request.input('address'),
        village_id: request.input('village_id'),
        zip_code: request.input('zip_code'),
        religion_id: request.input('religion_id'),
        phone: request.input('phone'),
        active: request.input('active', '0'),        
        registered_at: (request.input('registered_at', null) != '') ? moment(request.input('registered_at', null)).format('YYYY-MM-DD') : null
      })
      response.ok({ status: true, error: '', teacher: teacher })
    }
  }

  async update({ params, request, response }) {
    const rules = {
      nuptk: 'unique:teachers,nuptk,id,' + params.id,
      name: 'required'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      await Teacher.query().where('id', params.id).update({
        nuptk: request.input('nuptk'),
        name: request.input('name'),
        sex: request.input('sex'),
        birth_place: request.input('birth_place'),
        birth_date: (request.input('birth_date', null) != '') ? moment(request.input('birth_date', null)).format('YYYY-MM-DD') : null,
        address: request.input('address'),
        village_id: request.input('village_id'),
        zip_code: request.input('zip_code'),
        religion_id: request.input('religion_id'),
        phone: request.input('phone'),        
        active: request.input('active', '0'),        
        registered_at: (request.input('registered_at', null) != '') ? moment(request.input('registered_at', null)).format('YYYY-MM-DD') : null
      })
      response.ok({ status: true })
    }
  }

  async destroy({ params, response }) {
    const teacher = await Teacher.query().where('id', params.id).update({
      'deleted_at': moment().format('YYYY-MM-DD HH:mm:ss')
    })
    response.ok({ status: true })
  }

  async showActive({ request, response }) {
    const select = request.input('select', null)
    const sort = request.input('sort', null)

    const query = Teacher.query().where('active', '1')
    if (select != null) {
      const arrselect = select.split(',')
      query.select(arrselect)
    }
    if (sort != null) {
      const arrsort = sort.split(',')
      arrsort.forEach(element => {
        query.orderBy(element.split('|')[0].toLowerCase(), element.split('|').length == 2 ? element.split('|')[1].toLowerCase() : 'asc')
      })
    }
    const teachers = await query.orderBy('name')
    response.ok({
      status: true,
      data: {
        teachers: teachers
      }
    })
  }
}

module.exports = TeacherController
