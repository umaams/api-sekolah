'use strict'

const Village = use('App/Models/Village')
class DistrictController {

  async showVillages({ request, params, response }) {
    const villages = await Village.query().where('district_id', params.id).orderBy('name')
    return response.ok({ status: true, data: { villages: villages } })
  }
}

module.exports = DistrictController
