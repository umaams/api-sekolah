'use strict'

const { validateAll } = use('Validator')
const Lesson = use('App/Models/Lesson')
const moment = require('moment')

class LessonController {

  async index({ request, response }) {
    let status = true
    let error = ''

    const page = request.input('page', 1);
    const per_page = request.input('per_page', 10)
    const select = request.input('select', null)
    const filter = request.input('filter', null)
    const sort = request.input('sort', null)
    const include = request.input('include', null)

    let lessons = []
    let query = Lesson.query().whereNull('deleted_at')
    if (filter != null) {
      query.where(function () {
        this.orWhereRaw('LOWER(code) LIKE ?', ['%' + filter.toLowerCase() + '%'])
        this.orWhereRaw('LOWER(name) LIKE ?', ['%' + filter.toLowerCase() + '%'])
      })
    }
    if (select != null) {
      const arrselect = select.split(',')
      query.select(arrselect)
    }
    if (sort != null) {
      const arrsort = sort.split(',')
      arrsort.forEach(element => {
        query.orderBy(element.split('|')[0].toLowerCase(), element.split('|').length == 2 ? element.split('|')[1].toLowerCase() : 'asc')
      })
    }
    lessons = await query.paginate(page, per_page).catch(function (error) {
      status = false
      error = 'Server Internal Error:' + error.message
    })
    return response.ok({
      status: status,
      error: error,
      data: {
        lessons: lessons
      }
    })
  }

  async show({ request, params, response }) {
    const lesson = await Lesson.query().where('id', params.id).whereNull('deleted_at').first()
    response.ok({ status: true, error: '', data: { lesson: lesson } })
  }

  async store({ request, response }) {
    let status = false
    let error = ''

    const rules = {
      code: 'required',
      name: 'required'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      const lesson = await Lesson.create({
        code: request.input('code'),
        name: request.input('name'),
        active: request.input('active', '0')
      })
      response.ok({ status: true, error: '', lesson: lesson })
    }
  }

  async update({ params, request, response }) {
    const rules = {
      code: 'required',
      name: 'required'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      await Lesson.query().where('id', params.id).update({
        code: request.input('code'),
        name: request.input('name'),
        active: request.input('active', '0')        
      })
      response.ok({ status: true })
    }
  }

  async destroy({ params, response }) {
    const lesson = await Lesson.query().where('id', params.id).update({
      'deleted_at': moment().format('YYYY-MM-DD HH:mm:ss')
    })
    response.ok({ status: true })
  }

  async showCurriculumGradeNone({ request, params, response }) {
    const page = request.input('page', 1);
    const per_page = request.input('per_page', 10)
    const select = request.input('select', null)
    const filter = request.input('filter', null)
    const sort = request.input('sort', null)
    const curriculum_id = params.curriculum_id
    const grade_id = params.grade_id

    const query = Lesson.query().where('deleted_at', null).where('active', '1').whereDoesntHave('curriculums', (builder) => {
      builder.where('grade_id', grade_id).where('curriculum_id', curriculum_id)
    })
    if (filter != null) {
      query.where(function () {
        this.orWhereRaw('LOWER(code) LIKE ?', ['%' + filter.toLowerCase() + '%'])
        this.orWhereRaw('LOWER(name) LIKE ?', ['%' + filter.toLowerCase() + '%'])
      })
    }
    if (select != null) {
      const arrselect = select.split(',')
      query.select(arrselect)
    }
    if (sort != null) {
      const arrsort = sort.split(',')
      arrsort.forEach(element => {
        query.orderBy(element.split('|')[0].toLowerCase(), element.split('|').length == 2 ? element.split('|')[1].toLowerCase() : 'asc')
      })
    }
    const lessons = await query.paginate(page, per_page)

    response.ok({
      status: true,
      data: {
        lessons: lessons
      }
    })
  }
}

module.exports = LessonController
