'use strict'

const SchoolYear = use('App/Models/SchoolYear')

class SchoolYearController {

  async index({ request, params, response }) {
    const school_years = await SchoolYear.query().orderBy('name')
    return response.ok({ status: true, data: { school_years: school_years } })
  }

  async activate({ request, params, response }) {
    await SchoolYear.query().update({
      active: '0'
    })
    await SchoolYear.query().where('id', params.id).update({
      active: '1'
    })
    response.ok({ status: true })
  }
}

module.exports = SchoolYearController
