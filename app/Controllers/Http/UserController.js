'use strict'

const { validate } = use('Validator')
const User = use('App/Models/User')
const moment = require('moment')

class UserController {
  
  async index ({request, response}) {
    let status = true
    let error = ''

    const page = request.input('page', 1);
    const per_page = request.input('per_page', 10)
    const select = request.input('select', null)
    const filter = request.input('filter', null)
    const sort = request.input('sort', null)
    
    let users = []
    let query = User.query().whereNull('deleted_at')
    if (filter != null) {
      query.where(function () {
        this.orWhereRaw('LOWER(username) LIKE ?', ['%' + filter.toLowerCase() + '%'])
        this.orWhereRaw('LOWER(email) LIKE ?', ['%' + filter.toLowerCase() + '%'])
      })
    }
    if (select != null) {
      const arrselect = select.split(',')
      query.select(arrselect)
    }
    if (sort != null) {
      const arrsort = sort.split(',')
      arrsort.forEach(element => {
        query.orderBy(element.split('|')[0].toLowerCase(), element.split('|').length == 2 ? element.split('|')[1].toLowerCase() : 'asc')
      })
    }
    users = await query.paginate(page, per_page).catch(function (error) {
      status = false
      error = 'Server Internal Error:' + error.message
    })
    return response.ok({
      status: status,
      error: error,
      data: {
        users: users
      }
    })
  }

  async show({params, response}) {
    let status = true
    let error = ''

    let user = null
    user = await User.query().where('id', params.id).whereNull('deleted_at').first()
    response.ok({
      status: status, 
      error: error,
      data: {
        user: user
      } 
    })
  }

  async store({ request, response }) {
    const rules = {
      username: 'required|unique:users,username',
      email: 'required|email|unique:users,email',
      password: 'required|min:6',
      password_confirmation: 'required|min:6|same:password'
    }
    
    const validation = await validate(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      await User.create(request.only(['username', 'email', 'password']))
      response.ok({ status: true })
    }
  }

  async update({ params, request, response }) {
    const rules = {
      username: 'unique:users,username,id,' + params.id,
      email: 'unique:users,email,id,' + params.id
    }

    const validation = await validate(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    } else {
      await User.query().where('id', params.id).update({
        username: request.input('username'),
        email: request.input('email')
      })
      response.ok({ status: true })
    }
  }

  async destroy({ params, response }) {
    const user = await User.query().where('id', params.id).update({
      'deleted_at': moment().format('YYYY-MM-DD HH:mm:ss')
    })
    response.ok({ status: true })
  }
}

module.exports = UserController
