'use strict'

const Class = use('App/Models/Class')
const SchoolYear = use('App/Models/SchoolYear')
const { validateAll } = use('Validator')

class ClassGuardianController {

  async index ({request, response}) {
    const classes = await Class.query().with('class_guardian').whereNull('deleted_at').where('active', '1').fetch()
    
    return response.ok({status : true, data: { classes: classes }})
  }

  async attachGuardian ({request, params, response}) {
    const rules = {
      teacher_id: 'required'
    }
    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({
        status: false,
        errors: validation.messages()
      })
    }
    const active_school_year = await SchoolYear.query().where('active', '1').first()
    const kelas = await Class.find(params.id)
    await kelas.guardians().sync(request.input('teacher_id'), (row) => {
      row.school_year_id = active_school_year != null ? active_school_year.id : 0
    })
    return response.ok({ status: true }) 
  }

  async detachGuardian({ request, params, response }) {

    const rules = {
      teacher_id: 'required'
    }

    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.badRequest({ status: false, errors: validation.messages() })
    }
    const active_school_year = await SchoolYear.query().where('active', '1').first()
    const kelas = await Class.find(params.id)
    await kelas.guardians().detach(request.input('teacher_id'), (row) => {
      row.school_year_id = active_school_year != null ? active_school_year.id : 0
    })
    return response.ok({
      status: true
    })
  }
}

module.exports = ClassGuardianController
