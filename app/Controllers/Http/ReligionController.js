'use strict'

const Religion = use('App/Models/Religion')
class ReligionController {

  async index({ request, response }) {
    const religions = await Religion.query().orderBy('name')
    return response.ok({ status: true, data: { religions: religions } })
  }
}

module.exports = ReligionController
