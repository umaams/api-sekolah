'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')

Route.on('/').render('welcome')
Route.post('auth/login', 'AuthController.login')
Route.group(() => {
  Route.resource('user', 'UserController')
  Route.resource('student', 'StudentController')
  Route.get('student/class/none', 'StudentController.showClassNone')
  Route.get('teacher/active', 'TeacherController.showActive')
  Route.resource('teacher', 'TeacherController')
  Route.resource('class', 'ClassController')
  Route.resource('lesson', 'LessonController')
  Route.get('lesson/curriculum/:curriculum_id/grade/:grade_id/none', 'LessonController.showCurriculumGradeNone')

  Route.resource('curriculum', 'CurriculumController')
  Route.get('curriculum/:id/detail', 'CurriculumController.showDetails')
  Route.put('curriculum/attach/:curriculum_id', 'CurriculumController.attachLesson')
  Route.put('curriculum/detach/:curriculum_id', 'CurriculumController.detachLesson')
  Route.get('class_guardian', 'ClassGuardianController.index')
  Route.put('class_guardian/attach/:id', 'ClassGuardianController.attachGuardian')
  Route.put('class_guardian/detach/:id', 'ClassGuardianController.detachGuardian')
  Route.put('class_student/attach/:id', 'ClassStudentController.attachStudent')
  Route.put('class_student/detach/:id', 'ClassStudentController.detachStudent')
  Route.put('class_student/:id', 'ClassStudentController.syncStudent')
  Route.get('grade', 'GradeController.index')
  Route.get('grade/class/guardian', 'GradeController.showClassGuardian')
  Route.get('grade/:id/class/student', 'GradeController.showClassStudent')
  Route.get('grade/:id/class/lesson', 'GradeController.showClassLesson')
  Route.get('province', 'ProvinceController.index')
  Route.get('province/:id/city', 'ProvinceController.showCities')
  Route.get('city/:id/district', 'CityController.showDistricts')
  Route.get('district/:id/village', 'DistrictController.showVillages')
  Route.get('religion', 'ReligionController.index')
  Route.get('school_year', 'SchoolYearController.index')
  Route.put('school_year/activate/:id', 'SchoolYearController.activate')
}).middleware(['auth'])

