'use strict'

/*
|--------------------------------------------------------------------------
| GradeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Grade = use('App/Models/Grade')

class GradeSeeder {
  async run () {
    await Grade.query().delete()
    await Grade.createMany([
      { id: 1, name: 'I' },
      { id: 2, name: 'II' },
      { id: 3, name: 'III' },
      { id: 4, name: 'IV' },
      { id: 5, name: 'V' },
      { id: 6, name: 'VI'}
    ])
  }
}

module.exports = GradeSeeder
