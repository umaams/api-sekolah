'use strict'

/*
|--------------------------------------------------------------------------
| TeacherSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class TeacherSeeder {
  async run () {
    await Factory
      .model('App/Models/Teacher')
      .createMany(12)
  }
}

module.exports = TeacherSeeder
