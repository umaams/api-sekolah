'use strict'

/*
|--------------------------------------------------------------------------
| ClassSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database')
const Class = use('App/Models/Class')

class ClassSeeder {
  async run () {
    await Class.query().delete()    
    const classes = [
      {id: 1, name: 'I-A', grade_id: 1, active: '1'},
      {id: 2, name: 'II-A', grade_id: 2, active: '1'},
      {id: 3, name: 'III-A', grade_id: 3, active: '1'},
      {id: 4, name: 'IV-A', grade_id: 4, active: '1'},
      {id: 5, name: 'V-A', grade_id: 5, active: '1'},
      {id: 6, name: 'VI-A', grade_id: 6, active: '1'}
    ]
    await Class.createMany(classes)
  }
}

module.exports = ClassSeeder
