'use strict'

/*
|--------------------------------------------------------------------------
| CitySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database')
const Excel = require('exceljs')
const City = use('App/Models/City')

class CitySeeder {
  async run () {
  	let workbook = new Excel.Workbook();
    let cities = []
    await City.query().delete()
    await workbook.csv.readFile('database/seeds/cities.csv').then(function(worksheet) {
        worksheet.eachRow(function(row, rowNumber){
          if (row.getCell(1).value == '3516') {
            cities.push({
              id: row.getCell(1).value,
              province_id: row.getCell(2).value,
              name: row.getCell(3).value
            })
          }
        })
    })
    await City.createMany(cities)
  }
}

module.exports = CitySeeder
