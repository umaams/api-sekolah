'use strict'

/*
|--------------------------------------------------------------------------
| ReligionSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database') 
const Religion = use('App/Models/Religion')

class ReligionSeeder {
  async run () {
    await Religion.query().delete()    
    const religions = [
      {id: 1, name: 'Islam'},
      {id: 2, name: 'Kristen'},
      {id: 3, name: 'Katolik'},
      {id: 4, name: 'Hindu'},
      {id: 5, name: 'Budha'},
      {id: 6, name: 'Konghucu'},
      {id: 7, name: 'Lain-Lain'}
    ]
    await Religion.createMany(religions)
  }
}

module.exports = ReligionSeeder
