'use strict'

/*
|--------------------------------------------------------------------------
| ProvinceSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database')
const Excel = require('exceljs')
const Province = use('App/Models/Province')

class ProvinceSeeder {
  async run () {
  	let workbook = new Excel.Workbook();
    let provinces = []
    await Province.query().delete()
    await workbook.csv.readFile('database/seeds/provinces.csv').then(function(worksheet) {
        worksheet.eachRow(function(row, rowNumber){
          if (row.getCell(1).value == '35') {
            provinces.push({
              id: row.getCell(1).value,
              name: row.getCell(2).value
            })
          }
        })
    })
    await Province.createMany(provinces)
  }
}

module.exports = ProvinceSeeder
