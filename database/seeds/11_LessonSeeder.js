'use strict'

/*
|--------------------------------------------------------------------------
| LessonSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database')
const Lesson = use('App/Models/Lesson')

class LessonSeeder {
  async run () {
    await Lesson.query().delete()    
    const lessons = [
      {id: 1, code: 'BIN', name: 'BAHASA INDONESIA', active: '1'},
      {id: 2, code: 'MTK', name: 'MATEMATIKA', active: '1'},
      {id: 3, code: 'SAI', name: 'SAINS', active: '1'},
      {id: 4, code: 'BIG', name: 'BAHASA INGGRIS', active: '1'},
      {id: 5, code: 'AGM', name: 'AGAMA', active: '1'},
      {id: 6, code: 'PKN', name: 'PENDIDIKAN KEWARGANEGARAAN', active: '1'},
      {id: 7, code: 'IPS', name: 'ILMU PENGETAHUAN SOSIAL', active: '1'},
      {id: 8, code: 'KTK', name: 'KERAJINAN TANGAN & KESENIAN', active: '1'},
      {id: 9, code: 'OLG', name: 'PENDIDIKAN JASMANI & KESEHATAN', active: '1'},
      {id: 10, code: 'BJW', name: 'BAHASA JAWA', active: '1'}
    ]
    await Lesson.createMany(lessons)
  }
}

module.exports = LessonSeeder
