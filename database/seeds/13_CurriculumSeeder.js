'use strict'

/*
|--------------------------------------------------------------------------
| CurriculumSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database')
const Curriculum = use('App/Models/Curriculum')

class CurriculumSeeder {
  async run () {
    await Curriculum.query().delete()    
    const curriculums = [
      {id: 1, name: 'KURIKULUM 2013', active: '1'}
    ]
    await Curriculum.createMany(curriculums)
  }
}

module.exports = CurriculumSeeder
