'use strict'

/*
|--------------------------------------------------------------------------
| SchoolYearSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database') 
const SchoolYear = use('App/Models/SchoolYear')

class SchoolYearSeeder {
  async run () {
    await SchoolYear.query().delete()    
    const school_years = [
      { id: 1, name: '2017/2018', start_year: '2017', end_year: '2018', active: '1' },
      { id: 2, name: '2018/2019', start_year: '2018', end_year: '2019', active: '0' },
      { id: 3, name: '2019/2020', start_year: '2019', end_year: '2020', active: '0' },
      { id: 4, name: '2020/2021', start_year: '2020', end_year: '2021', active: '0' },
      { id: 5, name: '2021/2022', start_year: '2021', end_year: '2022', active: '0' }
    ]
    await SchoolYear.createMany(school_years)
  }
}

module.exports = SchoolYearSeeder
