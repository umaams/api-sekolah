'use strict'

/*
|--------------------------------------------------------------------------
| VillageSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database')
const Excel = require('exceljs')
const moment = require('moment')
const Village = use('App/Models/Village')

class VillageSeeder {
  async run () {
    let workbook = new Excel.Workbook();
    let villages = []
    await Village.query().delete()
    await workbook.csv.readFile('database/seeds/villages.csv').then(function(worksheet) {
        worksheet.eachRow(function(row, rowNumber){
          if (row.getCell(2).value == '3516150') {
            villages.push({
              id: row.getCell(1).value,
              district_id: row.getCell(2).value,
              name: row.getCell(3).value,
              created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
              updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
            })
          }
        })
    })
    //console.log(villages)
    await Database.table('villages').insert(villages)
  }
}

module.exports = VillageSeeder
