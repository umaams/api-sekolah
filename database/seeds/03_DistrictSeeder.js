'use strict'

/*
|--------------------------------------------------------------------------
| DistrictSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Database = use('Database')
const Excel = require('exceljs')
const District = use('App/Models/District')

class DistrictSeeder {
  async run () {
  	let workbook = new Excel.Workbook();
    let districts = []
    await District.query().delete()
    await workbook.csv.readFile('database/seeds/districts.csv').then(function(worksheet) {
        worksheet.eachRow(function(row, rowNumber){
          if (row.getCell(1).value == '3516150') {
            districts.push({
              id: row.getCell(1).value,
              city_id: row.getCell(2).value,
              name: row.getCell(3).value
            })
          }
        })
    })
    await District.createMany(districts)
  }
}

module.exports = DistrictSeeder
