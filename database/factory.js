'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

const Factory = use('Factory')
const moment = require('moment')

  Factory.blueprint('App/Models/User', (faker) => {
    return {
      username: faker.username(),
      email: faker.email(),
      password: '123456'
    }
  })

  Factory.blueprint('App/Models/Student', (faker) => {
    return {
      nis: faker.string({ pool: '1234567890', length: 6 }),
      name: faker.name(),
      sex: faker.character({pool: 'fm'}).toUpperCase(),
      birth_place: faker.city(),
      birth_date: moment(faker.birthday({string: true}), 'MM/DD/YYYY').format('YYYY-MM-DD'),
      address: faker.address(),
      village_id: '3516150005',
      zip_code: '61353',
      religion_id: 1,
      father_name: faker.name(),
      father_job: null,
      father_address: faker.address(),
      father_phone: '+' + faker.phone({ country: 'id', formatted: false }),
      mother_name: faker.name(),
      mother_job: null,
      mother_address: faker.address(),
      mother_phone: '+' + faker.phone({ country: 'id', formatted: false }),
      guardian_name: faker.name(),
      guardian_job: null,
      guardian_address: faker.address(),
      guardian_phone: '+' + faker.phone({ country: 'id', formatted: false }),
      registered_at: '2017-07-01',
      active: '1'
    }
  })

  Factory.blueprint('App/Models/Teacher', (faker) => {
    return {
      nuptk: faker.string({ pool: '1234567890', length: 10 }),
      name: faker.name(),
      sex: faker.character({ pool: 'fm' }).toUpperCase(),
      birth_place: faker.city(),
      birth_date: moment(faker.birthday({ string: true }), 'MM/DD/YYYY').format('YYYY-MM-DD'),
      address: faker.address(),
      village_id: '3516150005',
      zip_code: '61353',
      religion_id: 1,
      phone: '+' + faker.phone({ country: 'id', formatted: false }),
      registered_at: '2000-07-01',
      active: '1'
    }
  })


