'use strict'

const Schema = use('Schema')

class ClassLessonsSchema extends Schema {
  up () {
    this.create('class_lessons', (table) => {
      table.integer('school_year_id').unsigned().references('id').inTable('school_years')
      table.integer('class_id').unsigned().references('id').inTable('classes')
      table.integer('lesson_id').unsigned().references('id').inTable('lessons')
      table.integer('teacher_id').unsigned().references('id').inTable('teachers')
      table.timestamps()
    })
  }

  down () {
    this.drop('class_lessons')
  }
}

module.exports = ClassLessonsSchema
