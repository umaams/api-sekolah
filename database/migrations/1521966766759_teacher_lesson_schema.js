'use strict'

const Schema = use('Schema')

class TeacherLessonSchema extends Schema {
  up () {
    this.create('teacher_lessons', (table) => {
      table.integer('teacher_id').unsigned().references('id').inTable('teachers')
      table.integer('lesson_id').unsigned().references('id').inTable('lessons')
      table.integer('class_id').unsigned().references('id').inTable('classes')
      table.integer('school_year_id').unsigned().references('id').inTable('school_years')
      table.timestamps()
    })
  }

  down () {
    this.drop('teacher_lessons')
  }
}

module.exports = TeacherLessonSchema
