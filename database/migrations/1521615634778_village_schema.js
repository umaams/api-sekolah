'use strict'

const Schema = use('Schema')

class VillageSchema extends Schema {
  up () {
    this.create('villages', (table) => {
      table.bigInteger('id').unsigned().primary()
      table.string('name', 50).notNullable()
      table.bigInteger('district_id').unsigned().references('id').inTable('districts')
      table.timestamps()
    })
  }

  down () {
    this.drop('villages')
  }
}

module.exports = VillageSchema
