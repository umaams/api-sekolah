'use strict'

const Schema = use('Schema')

class CitySchema extends Schema {
  up () {
    this.create('cities', (table) => {
      table.bigInteger('id').unsigned().primary()
      table.string('name', 50).notNullable()
      table.bigInteger('province_id').unsigned().references('id').inTable('provinces')
      table.timestamps()
    })
  }

  down () {
    this.drop('cities')
  }
}

module.exports = CitySchema
