'use strict'

const Schema = use('Schema')

class LessonSchema extends Schema {
  up () {
    this.create('lessons', (table) => {
      table.increments()
      table.string('code', 10).notNullable()
      table.string('name', 50).notNullable()
      table.enu('active', ['0', '1']).notNullable().defaultTo('0')            
      table.timestamps()
      table.timestamp('deleted_at').nullable() 
    })
  }

  down () {
    this.drop('lessons')
  }
}

module.exports = LessonSchema
