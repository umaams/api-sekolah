'use strict'

const Schema = use('Schema')

class ClassSchema extends Schema {
  up () {
    this.create('classes', (table) => {
      table.increments()
      table.string('name', 20).notNullable()
      table.integer('grade_id').unsigned().references('id').inTable('grades')
      table.enu('active', ['0', '1']).notNullable().defaultTo('0')      
      table.timestamps()
      table.timestamp('deleted_at').nullable()     
    })
  }

  down () {
    this.drop('classes')
  }
}

module.exports = ClassSchema
