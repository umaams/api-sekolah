'use strict'

const Schema = use('Schema')

class ClassGuardianSchema extends Schema {
  up () {
    this.create('class_guardians', (table) => {
      table.integer('school_year_id').unsigned().references('id').inTable('school_years')
      table.integer('class_id').unsigned().references('id').inTable('classes')
      table.integer('teacher_id').unsigned().references('id').inTable('teachers')         
      table.timestamps()
    })
  }

  down () {
    this.drop('class_guardians')
  }
}

module.exports = ClassGuardianSchema
