'use strict'

const Schema = use('Schema')

class SchoolYearSchema extends Schema {
  up () {
    this.create('school_years', (table) => {
      table.increments()
      table.string('name', 12).notNullable()
      table.integer('start_year', 4).notNullable()
      table.integer('end_year', 4).notNullable()
      table.enu('active', ['0', '1']).notNullable().defaultTo('0')
      table.timestamps()
    })
  }

  down () {
    this.drop('school_years')
  }
}

module.exports = SchoolYearSchema
