'use strict'

const Schema = use('Schema')

class ReligionSchema extends Schema {
  up () {
    this.create('religions', (table) => {
      table.increments()
      table.string('name', 20).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('religions')
  }
}

module.exports = ReligionSchema
