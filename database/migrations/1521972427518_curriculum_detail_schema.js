'use strict'

const Schema = use('Schema')

class CurriculumDetailSchema extends Schema {
  up () {
    this.create('curriculum_details', (table) => {
      table.increments()
      table.integer('curriculum_id').unsigned().references('id').inTable('curriculums')
      table.integer('lesson_id').unsigned().references('id').inTable('lessons')
      table.integer('grade_id').unsigned().references('id').inTable('grades')
      table.timestamps()
    })
  }

  down () {
    this.drop('curriculum_details')
  }
}

module.exports = CurriculumDetailSchema
