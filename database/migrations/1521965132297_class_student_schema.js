'use strict'

const Schema = use('Schema')

class ClassStudentSchema extends Schema {
  up () {
    this.create('class_students', (table) => {
      table.integer('class_id').unsigned().references('id').inTable('classes')
      table.integer('student_id').unsigned().references('id').inTable('students')
      table.integer('school_year_id').unsigned().references('id').inTable('school_years')
      table.timestamps()
    })
  }

  down () {
    this.drop('class_students')
  }
}

module.exports = ClassStudentSchema
