'use strict'

const Schema = use('Schema')

class CurriculumSchema extends Schema {
  up () {
    this.create('curriculums', (table) => {
      table.increments()
      table.string('name', 30).notNullable()
      table.enu('active', ['0', '1']).notNullable().defaultTo('0')
      table.timestamps()
    })
  }

  down () {
    this.drop('curriculums')
  }
}

module.exports = CurriculumSchema
