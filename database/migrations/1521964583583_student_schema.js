'use strict'

const Schema = use('Schema')

class StudentSchema extends Schema {
  up () {
    this.create('students', (table) => {
      table.increments()
      table.string('nis', 50).notNullable().unique()
      table.string('name', 80).notNullable()
      table.enu('sex', ['M', 'F']).nullable()
      table.string('birth_place', 50).nullable()
      table.date('birth_date').nullable()
      table.text('address').nullable()
      table.bigInteger('village_id').unsigned().references('id').inTable('villages')
      table.string('zip_code', 8).nullable()
      table.integer('religion_id').unsigned().references('id').inTable('religions')
      table.string('father_name', 80).nullable()
      table.string('father_job', 50).nullable()
      table.text('father_address').nullable()
      table.string('father_phone', 12).nullable()
      table.string('mother_name', 80).nullable()
      table.string('mother_job', 50).nullable()
      table.text('mother_address').nullable()
      table.string('mother_phone', 12).nullable()
      table.string('guardian_name', 80).nullable()
      table.string('guardian_job', 50).nullable()
      table.text('guardian_address').nullable()
      table.string('guardian_phone', 12).nullable()
      table.date('registered_at').nullable()
      table.enu('active', ['0', '1']).notNullable().defaultTo('0')              
      table.timestamps()
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('students')
  }
}

module.exports = StudentSchema
