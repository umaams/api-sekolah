'use strict'

const Schema = use('Schema')

class TeacherSchema extends Schema {
  up () {
    this.create('teachers', (table) => {
      table.increments()
      table.string('nuptk', 50).notNullable().unique()
      table.string('name', 80).notNullable()
      table.enu('sex', ['M', 'F']).nullable()
      table.string('birth_place', 50).nullable()
      table.date('birth_date').nullable()
      table.text('address').nullable()
      table.bigInteger('village_id').unsigned().references('id').inTable('villages')
      table.string('zip_code', 8).nullable()
      table.integer('religion_id').nullable()
      table.string('phone', 12).nullable()
      table.date('registered_at').nullable()
      table.enu('active', ['0', '1']).notNullable().defaultTo('0')        
      table.timestamps()
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.drop('teachers')
  }
}

module.exports = TeacherSchema
