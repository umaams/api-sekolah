'use strict'

const Schema = use('Schema')

class DistrictSchema extends Schema {
  up () {
    this.create('districts', (table) => {
      table.bigInteger('id').unsigned().primary()
      table.string('name', 50).notNullable()
      table.bigInteger('city_id').unsigned().references('id').inTable('cities')
      table.timestamps()
    })
  }

  down () {
    this.drop('districts')
  }
}

module.exports = DistrictSchema
